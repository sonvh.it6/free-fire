import { CommonState } from './types';
import { SlideName, Orientation } from './const';

export const initState = (): CommonState => ({
  canSwipeToOtherSlide: true,
  currentSlide: SlideName.Visual,
  orientation: Orientation.Portrait,
  modalLoginIsOpen: false,
  modalMvIsOpen: false,
  modalLegendaryTemplesIsOpen: false,
  modalShareIsOpen: false,
  shareUrls: {
    facebook: '',
    instagram: '',
    zalo: '',
  },
});

export default initState;
