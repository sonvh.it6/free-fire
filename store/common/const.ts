import { namespace } from 'vuex-class';

/**
 * Polls namespace for vuex-class injection
 */
export const commonModule = namespace('common/');

export enum SlideName {
  Visual = 1,
  Introduce = 2,
  DrawableImage = 3,
}

export enum Orientation {
  Portrait = 0,
  Landscape = 1,
}

export interface Sharer {
  facebook: string;
  instagram: string;
  zalo: string;
}
