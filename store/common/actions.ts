import { CommonActions } from './types';

export const actions: CommonActions = {
  canSwipe({ commit, state }, swiped) {
    if (typeof swiped !== 'undefined') {
      commit('setSwipe', swiped);
    } else {
      commit('setSwipe', !state.canSwipeToOtherSlide);
    }
  },
  changeToSlide({ commit }, slide) {
    commit('setSlide', slide);
  },
  changeOrientation({ commit }, orientation) {
    commit('setOrientation', orientation);
  },
  toggleLoginModal({ commit, state }, isOpen?) {
    if (typeof isOpen !== 'undefined') {
      commit('setModalLogin', isOpen);
    } else {
      commit('setModalLogin', !state.modalLoginIsOpen);
    }
  },
  toggleMvModal({ commit, state }, isOpen?) {
    if (typeof isOpen !== 'undefined') {
      commit('setModalMv', isOpen);
    } else {
      commit('setModalMv', !state.modalMvIsOpen);
    }
  },
  toggleTempleModal({ commit, state }, isOpen?) {
    if (typeof isOpen !== 'undefined') {
      commit('setModalTemple', isOpen);
    } else {
      commit('setModalTemple', !state.modalLegendaryTemplesIsOpen);
    }
  },
  toggleShareModal({ commit, state }, isOpen?) {
    if (typeof isOpen !== 'undefined') {
      commit('setModalShare', isOpen);
    } else {
      commit('setModalShare', !state.modalShareIsOpen);
    }
  },
  assignShareUrls({ commit }, shareUrls) {
    commit('setShareUrls', shareUrls);
  },
};

export default actions;
