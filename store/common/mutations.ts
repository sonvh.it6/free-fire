import { CommonMutations } from './types';

export const mutations: CommonMutations = {
  setSwipe(state, swiped) {
    state.canSwipeToOtherSlide = swiped;
  },
  setSlide(state, slide) {
    state.currentSlide = slide;
  },
  setOrientation(state, orientation) {
    console.log(orientation);
    state.orientation = orientation;
  },
  setModalLogin(state, isOpen) {
    state.modalLoginIsOpen = isOpen;
  },
  setModalMv(state, isOpen) {
    state.modalMvIsOpen = isOpen;
  },
  setModalTemple(state, isOpen) {
    state.modalLegendaryTemplesIsOpen = isOpen;
  },
  setModalShare(state, isOpen) {
    state.modalShareIsOpen = isOpen;
  },
  setShareUrls(state, shareUrls) {
    state.shareUrls = shareUrls;
  },
};

export default mutations;
