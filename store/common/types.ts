import { ActionTree, MutationTree, ActionContext } from 'vuex';
import { RootState } from '../types';
import { SlideName, Orientation, Sharer } from './const';

export interface CommonState {
  currentSlide: SlideName;
  orientation: number;
  modalLoginIsOpen: boolean;
  modalMvIsOpen: boolean;
  modalLegendaryTemplesIsOpen: boolean;
  modalShareIsOpen: boolean;
  shareUrls: Sharer;
  canSwipeToOtherSlide: boolean;
}

export type CommonActionContext = ActionContext<CommonState, RootState>;

export interface CommonActions extends ActionTree<CommonState, RootState> {
  canSwipe(ctx: CommonActionContext, swiped?: boolean);
  changeToSlide(ctx: CommonActionContext, slide: SlideName);
  changeOrientation(ctx: CommonActionContext, orientation: Orientation);
  toggleLoginModal(ctx: CommonActionContext, isOpen?: boolean);
  toggleMvModal(ctx: CommonActionContext, isOpen?: boolean);
  toggleTempleModal(ctx: CommonActionContext, isOpen?: boolean);
  toggleShareModal(ctx: CommonActionContext, isOpen?: boolean);
  assignShareUrls(ctx: CommonActionContext, shareUrls: Sharer);
}

export interface CommonMutations extends MutationTree<CommonState> {
  setSwipe(state: CommonState, swiped: boolean);
  setSlide(state: CommonState, slide: number);
  setOrientation(state: CommonState, orientation: Orientation);
  setModalLogin(state: CommonState, isOpen: boolean);
  setModalMv(state: CommonState, isOpen: boolean);
  setModalTemple(state: CommonState, isOpen: boolean);
  setModalShare(state: CommonState, isOpen: boolean);
  setShareUrls(state: CommonState, shareUrls: Sharer);
}
