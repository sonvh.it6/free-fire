import { namespace } from 'vuex-class';

/**
 * Polls namespace for vuex-class injection
 */
export const uploadModule = namespace('upload/');
