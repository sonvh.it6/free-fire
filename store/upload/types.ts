import { ActionTree, MutationTree, ActionContext, GetterTree } from 'vuex';
import { RootState } from '../types';

export interface UploadState {
  uploadedImageUrl: string;
  userHasUploadedImage: boolean;
  listLegendaryTemplesImg: string[];
}

export type UploadActionContext = ActionContext<UploadState, RootState>;

export interface UploadActions extends ActionTree<UploadState, RootState> {
  uploadImage(ctx: UploadActionContext, image: string);
  getAllTempleImages(ctx: UploadActionContext);
}

export interface UploadMutations extends MutationTree<UploadState> {
  setUploadedImageUrl(state: UploadState, url: string);
  setUserHasUploadedImage(state: UploadState, uploaded: boolean);
  setListTempleImages(state: UploadState, list: string[]);
}

export type UploadGetters = GetterTree<UploadState, RootState>;
