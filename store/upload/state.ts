import { UploadState } from './types';

export const initState = (): UploadState => ({
  uploadedImageUrl: '',
  userHasUploadedImage: false,
  listLegendaryTemplesImg: [],
});

export default initState;
