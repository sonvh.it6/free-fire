import { UploadGetters } from './types';

export const getters: UploadGetters = {
  getUploadedImages(state) {
    return group => {
      let results: string[][] = [];
      let groupItems: string[] = [];
      let counter = 0;

      for (let item of state.listLegendaryTemplesImg) {
        if (counter === group) {
          results.push(groupItems);
          groupItems = [];
          counter = 0;
        }
        groupItems.push(item);
        counter++;
      }

      if (groupItems.length) {
        results.push(groupItems);
      }

      return results;
    };
  },
};

export default getters;
