import { UploadMutations } from './types';

export const mutations: UploadMutations = {
  setUserHasUploadedImage(state, uploaded) {
    state.userHasUploadedImage = uploaded;
  },
  setUploadedImageUrl(state, url) {
    state.uploadedImageUrl = url;
  },
  setListTempleImages(state, list) {
    state.listLegendaryTemplesImg = list;
  },
};

export default mutations;
