import jwt from 'jsonwebtoken';
import axios from 'axios';
import { UploadActions } from './types';
import { SECRET } from '../../constants';

export const actions: UploadActions = {
  uploadImage({ commit, state }, image) {
    if (!state.userHasUploadedImage) {
      let token = jwt.sign({ image }, SECRET, { expiresIn: '1m' });
      commit('setUserHasUploadedImage', true);
      return axios.post('/api/upload', { token }).then(response => {
        commit('setUploadedImageUrl', response.data.data);
        return response.data.data;
      });
    } else {
      return state.uploadedImageUrl;
    }
  },
  getAllTempleImages({ commit }) {
    axios.get('/api/uploaded-images').then(response => {
      commit('setListTempleImages', response.data.data);
    });
  },
};

export default actions;
