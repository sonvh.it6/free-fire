require('dotenv').config();

module.exports = {
  mode: 'spa',
  server: {
    port: 3000,
    host: '0.0.0.0',
  },
  /*
   ** Headers of the page
   */
  head: {
    title: 'Free Fire',
    meta: [
      { charset: 'utf-8' },
      {
        name: 'viewport',
        content:
          'width=device-width,, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0, viewport-fit=cover',
      },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.png' },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Montserrat:400,600i,900i&display=block',
      },
    ],
    script: [
      { src: '/js/jquery-3.4.1.min.js', body: true },
      { src: '/js/jquery.fullpage.min.js', body: true },
      { src: '/js/load-image.all.min.js', body: true },
    ],
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: ['./assets/css/animate.css', './assets/css/swiper.min.css'],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: ['~/plugins/font-awesome.js', '~/plugins/register-components.js'],
  /*
   ** Nuxt.js modules
   */
  buildModules: ['@nuxt/typescript-build'],
  modules: ['@nuxtjs/dotenv', '@nuxtjs/font-awesome', '@nuxtjs/style-resources'],
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      config.node = {
        fs: 'empty',
        net: 'empty',
        tls: 'empty',
      };
    },
  },

  styleResources: {
    scss: [
      './assets/scss/_responsive.scss',
      './assets/scss/_variables.scss',
      './assets/scss/_fonts.scss',
      './assets/scss/_animate.scss',
    ],
  },
  serverMiddleware: ['~/server/api'],
};
