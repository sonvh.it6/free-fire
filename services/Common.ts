import MobileDetect from 'mobile-detect';

export class CommonService {
  static isMobile() {
    const md = new MobileDetect(window.navigator.userAgent);
    const os = md.os();
    return os === 'AndroidOS' || os === 'iOS';
  }

  static convertUrlToData(url) {
    return fetch(url)
      .then(response => response.blob())
      .then(blob => URL.createObjectURL(blob));
  }

  static async downloadImageFromUrl(url: string, filename?: string) {
    if (CommonService.isMobile()) {
      window.open(url, '_blank');
    } else {
      let data = await CommonService.convertUrlToData(url);
      CommonService.downloadImageFromData(data, filename);
    }
  }

  static async downloadImageFromData(data: string, filename?: string) {
    if (CommonService.isMobile()) {
      window.open(data, '_blank');
    } else {
      const a = document.createElement('a');
      a.href = data;
      a.download = `${filename || 'freefire'}.jpg`;
      document.body.appendChild(a);
      a.click();
      document.body.removeChild(a);
    }
  }

  static shareToFacebook(url) {
    window.open(url, '_blank', 'height=500,width=300');
  }
}
