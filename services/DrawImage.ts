export class DrawImageService {
  static draw(src: string): Promise<HTMLImageElement> {
    return new Promise((resolve, reject) => {
      const img = new Image();
      img.src = src;
      img.onload = () => {
        resolve(img);
      };
    });
  }

  static drawFromFile(file: File): Promise<HTMLImageElement> {
    return new Promise((resolve, reject) => {
      var reader = new FileReader();
      reader.readAsDataURL(file);

      reader.onloadend = (e: any) => {
        const img = new Image();
        if (e.target && e.target.result) {
          img.src = e.target.result;
        }
        img.onload = () => {
          resolve(img);
        };
      };
    });
  }

  static resizeBase64Canvas(canvas, w, h) {
    const resizedCanvas = document.createElement('canvas');
    const resizedContext = resizedCanvas.getContext('2d');

    resizedCanvas.width = w;
    resizedCanvas.height = h;

    if (resizedContext) {
      resizedContext.drawImage(canvas, 0, 0, w, h);
      return resizedCanvas.toDataURL();
    }
    return null;
  }
}
