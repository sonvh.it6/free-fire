export const ANDROID_URL = 'https://play.google.com/store/apps/details?id=com.dts.freefireth';
export const IOS_URL = 'https://itunes.apple.com/vn/app/garena-free-fire/id1300146617?mt=8';
export const KOLS_PORTRAIT_VISUALS = [
  require('./assets/img/visuals/visual-1.jpg'),
  require('./assets/img/visuals/visual-2.jpg'),
  require('./assets/img/visuals/visual-3.jpg'),
];
export const KOLS_LANDSCAPE_VISUALS = [
  require('./assets/img/visuals/visual-landscape-1.jpg'),
  require('./assets/img/visuals/visual-landscape-2.jpg'),
  require('./assets/img/visuals/visual-landscape-3.jpg'),
];
export const VISUAL_SLIDE_FADE_TIMEOUT = 4000;
export const INTRODUCE_SLIDE_FADE_TIMEOUT = 800;
export const YOUTUBE_MV_URL = 'https://www.youtube.com/watch?v=mlFiwKicz7I';
export const SHARE_HASHTAG = 'SongDaiThanhHuyenThoai';
export const SECRET = 'cclmcfxgQAbWsc9l_Utu2ReZ1cDAu';
