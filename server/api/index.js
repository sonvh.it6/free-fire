import express from 'express';
import cors from 'cors';
import uuid from 'uuid/v4';
import jwt from 'jsonwebtoken';
import fs from 'fs';

const app = express();

app.use(express.json({ limit: '50mb', extended: true }));
app.use(cors());

app.get('/uploaded-images', (req, res) => {
  try {
    let listImages = [];
    let defaultKolPath = `${__dirname}/../../static/img/kols/`;
    let userPath = `${__dirname}/../../static/img/legendary-temples/`;

    let defaultKolImgs = fs
      .readdirSync(defaultKolPath)
      .filter(v => v.indexOf('jpg') > -1)
      .map(v => ({ name: v, time: fs.statSync(defaultKolPath + v).mtime.getTime() }))
      .sort((a, b) => b.time - a.time)
      .map(v => v.name);

    for (let file of defaultKolImgs) {
      listImages.push(`/img/kols/${file}`);
    }

    let userImgs = fs
      .readdirSync(userPath)
      .filter(v => v.indexOf('jpg') > -1)
      .map(v => ({ name: v, time: fs.statSync(userPath + v).mtime.getTime() }))
      .sort((a, b) => b.time - a.time)
      .map(v => v.name);

    for (let file of userImgs) {
      listImages.push(`/img/legendary-temples/${file}`);
    }

    res.json({ successful: true, data: listImages });
  } catch (error) {
    res.status(500).json({ successful: false, message: error.message });
  }
});

app.post('/upload', (req, res) => {
  try {
    if (!req.body.token) {
      throw new Error('Token missing');
    }

    const { token } = req.body;
    let data = jwt.verify(token, process.env.SECRET);
    let filename = uuid() + '.jpg';
    let image = data.image.replace(/^data:image\/jpeg;base64,/, '');

    fs.writeFileSync(`${__dirname}/../../static/img/legendary-temples/${filename}`, image, 'base64');

    res.status(200).json({ successful: true, data: '/img/legendary-temples/' + filename });
  } catch (error) {
    res.status(401).json({ successful: false, message: error.message });
  }
});

export default {
  path: '/api',
  handler: app,
};
