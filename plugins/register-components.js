import Vue from 'vue';
import VueAwesomeSwiper from 'vue-awesome-swiper';
import VueSlider from 'vue-slider-component';
import 'vue-slider-component/theme/antd.css';
import 'swiper/dist/css/swiper.css';

Vue.component('VueSlider', VueSlider);
Vue.use(VueAwesomeSwiper);
